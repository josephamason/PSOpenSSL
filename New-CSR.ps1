﻿function New-CSR
{
  param
  (
    [string]$Path,
    [string]$OpenSSLPath
  )
  Begin
  {
    $PathTP = Test-Path -Path $Path -PathType Leaf
    if($PathTP)
    {
      $CSV = Import-Csv -Path $Path
    }
  }
  Process
  {
    foreach($Item in $CSV)
    {
      
      if($Item.SAN)
      {

        [string[]]$SANs = $null
        foreach($SAN in $Item.SAN -split ';')
        {
          $SANs += "DNS: $SAN"
        }
      }
if($Item.SAN)
{
  $OSSLConf = @"
[ req ]
default_bits = 2048
prompt = no
encrypt_key = no
default_md = sha256
distinguished_name = dn
req_extensions = req_ext

[ dn ]
CN = $($Item.CommonName)
emailAddress = $($Item.Email)
O = Puget Sound Energy, Inc.
OU = $($Item.Team)
L = City
ST = State
C = US

[ req_ext ]
subjectAltName = $($SANs -join ',')
"@
}
else
{
  $OSSLConf = @"
[ req ]
default_bits = 2048
prompt = no
encrypt_key = no
default_md = sha256
distinguished_name = dn

[ dn ]
CN = $($Item.CommonName)
emailAddress = $($Item.Email)
O = Puget Sound Energy, Inc.
OU = $($Item.Team)
L = City
ST = State
C = US
"@
}
      
      
      $ParentPath = Split-Path -Path $Path -Parent
      $CNPath = "$ParentPath\$($Item.CommonName)"
      $CNPathTP = Test-Path -Path $CNPath

      if(-not $CNPathTP)
      {
        New-Item -ItemType Directory -Path $ParentPath -Name $($Item.CommonName)|Out-Null
      }

      $ConfigPath = "$CNPath\$($Item.CommonName).conf"
      $KeyPath = "$CNPath\$($Item.CommonName).key"
      $CSRPath = "$CNPath\$($Item.CommonName).csr"

      $OSSLConf.ToString()|Out-File -FilePath $ConfigPath -Encoding ascii
      $SPOSSLArgs = "req -new -config '$ConfigPath' -keyout '$KeyPath' -out '$CSRPath'"

      $ErrorActionPreference = 'SilentlyContinue'
      Invoke-Expression -Command "$OpenSSLPath $SPOSSLArgs"
      $ErrorActionPreference = 'Continue'
    }
  }
}

New-CSR -Path "D:\temp\Certs\temp\Certs.csv" -OpenSSLPath "C:\Users\jamaso\Downloads\openssl-0.9.8h-1-bin\bin\openssl.exe"