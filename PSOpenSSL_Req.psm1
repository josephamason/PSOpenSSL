﻿function New-OSSLReq
{
<#
	.SYNOPSIS
		A brief description of the New-OSSLReq function.
	
	.DESCRIPTION
		This option generates a new certificate request. It will prompt the user for the relevant field values. The actual fields prompted for and their maximum and minimum sizes are specified in the configuration file and any requested extensions.
		
		If the -key option is not used it will generate a new RSA private key using information specified in the configuration file.
	
	.PARAMETER Key
		This specifies the file to read the private key from. It also accepts PKCS#8 format private keys for PEM format files.
	
	.PARAMETER KeyOut
		This gives the filename to write the newly created private key to. If this option is not specified then the filename present in the configuration file is used.
	
	.PARAMETER CommonName
		A description of the CommonName parameter.
	
	.PARAMETER Config
		This allows an alternative configuration file to be specified.
	
	.PARAMETER OrganizationName
		A description of the OrganizationName parameter.
	
	.PARAMETER stateOrProvinceName
		A description of the stateOrProvinceName parameter.
	
	.PARAMETER countryName
		A description of the countryName parameter.
	
	.PARAMETER localityName
		A description of the localityName parameter.
	
	.PARAMETER emailAddress
		A description of the emailAddress parameter.
	
	.PARAMETER BitSize
		A description of the BitSize parameter.
	
	.PARAMETER OutPath
		A description of the OutPath parameter.
	
	.PARAMETER CSROut
		A description of the CSROut parameter.
	
	.EXAMPLE
		PS C:\> New-OSSLReq
	
	.NOTES
		Additional information about the function.
	
	.LINK
		OpenSSL CONF library configuration files
		https://www.openssl.org/docs/manmaster/man5/config.html
#>
	
	[CmdletBinding(DefaultParameterSetName = 'DeclaredKeyOut')]
	param
	(
		[Parameter(ParameterSetName = 'ConfigKey')]
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[string]$Key,
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[Parameter(ParameterSetName = 'ConfigKeyOut')]
		[switch]$KeyOut,
		[Parameter(ParameterSetName = 'ConfigKeyOut')]
		[Parameter(ParameterSetName = 'ConfigKey')]
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut',
				   Mandatory = $true)]
		[string]$CommonName,
		[Parameter(ParameterSetName = 'ConfigKey')]
		[Parameter(ParameterSetName = 'ConfigKeyOut')]
		[string]$Config,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[string]$OrganizationName,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[string]$stateOrProvinceName,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[string]$countryName,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[string]$localityName,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[string]$emailAddress,
		[Parameter(ParameterSetName = 'DeclaredKey')]
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[ValidateSet('512', '1024', '2048', '4096', '8192')]
		[int]$BitSize,
		[Parameter(ParameterSetName = 'DeclaredKeyOut')]
		[Parameter(ParameterSetName = 'ConfigKeyOut')]
		[Parameter(ParameterSetName = 'ConfigKey')]
		[Parameter(ParameterSetName = 'DeclaredKey',
				   Mandatory = $true)]
		[string]$OutPath
	)
	
	Begin
	{
		$OpenSSLPath = "C:\Users\jamaso\Downloads\openssl-0.9.8h-1-bin\bin\openssl.exe"
		$ConfigTemplate = "D:\Dev\Puget-ESS-Powershell\Modules\PSOpenSSL\req_san.txt"
	}
	Process
	{
		$OutPathTP = Test-Path -Path $OutPath -PathType Container
		if ($OutPathTP)
		{
			$CSROut = "$($OutPathTP)\$($CommonName).csr"
			$KeyOut = "$($OutPathTP)\$($CommonName).key"
			
			if ($PSCmdlet.ParameterSetName -eq 'ConfigKeyOut')
			{
				
			}
			
			if ($PSCmdlet.ParameterSetName -eq 'DeclaredKeyOut')
			{
				
			}
			
			if ($PSCmdlet.ParameterSetName -eq 'ConfigKey')
			{
				$KeyTP = Test-Path -Path $key -PathType Leaf
			}
			
			if ($PSCmdlet.ParameterSetName -eq 'DeclaredKey')
			{
				$KeyTP = Test-Path -Path $key -PathType Leaf
			}
			
			switch ($PSCmdlet.ParameterSetName)
			{
				'ConfigKey'{ Start-Process -FilePath $OpenSSLPath -ArgumentList "req -new -config '$Config' -key '$Key' -out '$CSROut'" }
				'ConfigKeyOut'{ Start-Process -FilePath $OpenSSLPath -ArgumentList "req -new -config '$Config' -keyout '$KeyOut' -out '$CSROut'" }
				'DeclaredKey'{ Start-Process -FilePath $OpenSSLPath -ArgumentList "req -new -config '$Config' -key '$Key' -out '$CSROut'" }
				'DeclaredKeyOut'{ Start-Process -FilePath $OpenSSLPath -ArgumentList "req -new -config '$Config' -keyout '$KeyOut' -out '$CSROut'" }
			}
		}
	}
}
function Show-OSSLReq
{
	[CmdletBinding()]
	param ()
	
	#TODO: Place script here
}
